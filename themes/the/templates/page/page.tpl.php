<!--.page -->
<div class="page" role="document">

  <!--.l-header -->
  <header class="l-header" role="banner">
    <div class="l-header__inner">

      <?php if ($linked_logo || $linked_site_name): ?>
        <div class="l-header__logo">
          <?php if ($linked_logo): ?>
            <?php print $linked_logo; ?>
            <?php elseif ($linked_site_name): ?>
            <?php print $linked_site_name; ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <!-- Site Sloagn -->
      <div class="site-slogan">
          <span><?php print variable_get('site_slogan'); ?></span>
      </div>
      <!-- Site Slogan -->
      <?php if (!empty($page['header_contact_details'])): ?>
        <div class="l-header__contact-details">
          <?php print render($page['header_contact_details']); ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($page['header'])): ?>
        <?php print render($page['header']); ?>
      <?php endif; ?>

    </div>
  </header>
  <!--/.l-header -->

  <?php if ($top_bar): ?>
    <!--.nav -->
    <div class="nav<?php if ($top_bar_classes): print ' ' . $top_bar_classes; endif; ?>">
      <nav class="top-bar" data-topbar <?php print $top_bar_options; ?>>
        <ul class="title-area">
          <li class="name"></li>
          <li class="toggle-topbar menu-icon">
            <a href="#"><span><?php print $top_bar_menu_text; ?></span></a></li>
        </ul>
        <section class="top-bar-section">
          <?php if ($top_bar_main_menu) : ?>
            <?php print $top_bar_main_menu; ?>
          <?php endif; ?>
        </section>
      </nav>
    </div>
    <!--/.nav -->
  <?php endif; ?>

  <?php if (!empty($page['featured'])): ?>
    <!--.l-featured -->
    <section class="l-featured">
      <?php print render($page['featured']); ?>
    </section>
    <!--/.l-featured -->
  <?php endif; ?>

  <?php if ($messages && !$zurb_foundation_messages_modal): ?>
    <!--.l-messages -->
    <section class="l-messages">
      <?php if ($messages): print $messages; endif; ?>
    </section>
    <!--/.l-messages -->
  <?php endif; ?>

  <?php if (!empty($page['help'])): ?>
    <!--.l-help -->
    <section class="l-help">
      <?php print render($page['help']); ?>
    </section>
    <!--/.l-help -->
  <?php endif; ?>

  <?php if ($breadcrumb): ?>
    <!--.l-breadcrumbs -->
    <div class="l-breadcrumbs">
      <div class="l-breadcrumbs__list">
        <?php print $breadcrumb; ?>
      </div>
      <div class="l-breadcrumbs__title">
        <?php if ($title_location): ?>
          <h1 class="page__title page__title--location"><?php print $title_location; ?></h1>
        <?php endif; ?>
      </div>
    </div>
    <!--/.l-breadcrumbs -->
  <?php endif; ?>

  <!--.l-main -->
  <main class="l-main" role="main">
    <div class="l-main__inner">

      <div class="l-main-content <?php print $main_content; ?>">
        <?php if (!empty($page['highlighted'])): ?>
          <div class="highlight panel callout">
            <?php print render($page['highlighted']); ?>
          </div>
        <?php endif; ?>

        <a id="main-content"></a>

        <?php if ($title): ?>
          <?php print render($title_prefix); ?>
          <h2 class="page__title" id="page-title"><?php print $title; ?></h2>
          <?php print render($title_suffix); ?>
        <?php endif; ?>

        <?php if (!empty($tabs)): ?>
          <div class="action-tabs">
            <?php print render($tabs); ?>
            <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
          </div>
        <?php endif; ?>

        <?php if ($action_links): ?>
          <ul class="action-links">
            <?php print render($action_links); ?>
          </ul>
        <?php endif; ?>

        <?php print render($page['content']); ?>
      </div>

      <?php if (!empty($page['sidebar'])): ?>
        <aside class="l-sidebar" role="complementary">
          <?php print render($page['sidebar']); ?>
        </aside>
      <?php endif; ?>

    </div>
  </main>
  <!--/.l-main -->

  <?php if (!empty($page['footer_first']) || !empty($page['footer_second']) || !empty($page['footer_third']) || !empty($page['footer_fourth'])): ?>
    <!--.l-footer -->
    <footer class="l-footer" role="contentinfo">
      <div class="l-footer__inner">

        <?php if (!empty($page['footer_first'])): ?>
          <div class="l-footer__first">
            <?php print render($page['footer_first']); ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($page['footer_second'])): ?>
          <div class="l-footer__second">
            <?php print render($page['footer_second']); ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($page['footer_third'])): ?>
          <div class="l-footer__third">
            <?php print render($page['footer_third']); ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($page['footer_fourth'])): ?>
          <div class="l-footer__fourth">
            <?php print render($page['footer_fourth']); ?>
          </div>
        <?php endif; ?>

      </div>
    </footer>
    <!--/.l-footer -->
  <?php endif; ?>

  <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
</div>
<!--/.page -->

<?php if (!empty($page['outer_content'])): ?>
  <?php print render($page['outer_content']); ?>
<?php endif; ?>
